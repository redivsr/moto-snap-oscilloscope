/**
 * Health Mod Driver implementation
 * by Venturus
 * 
 * All structs used are define in nuttx/include/nuttx/device.h
 */

#define CONFIG_SCHED_LPWORKPERIOD 5000

#include <errno.h>
#include <debug.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <arch/byteorder.h>
#include <arch/board/board.h>

#include <nuttx/arch.h>
#include <nuttx/analog/adc.h>
#include <nuttx/clock.h>
#include <nuttx/gpio.h>
#include <nuttx/greybus/debug.h>
#include <nuttx/device.h>
#include <nuttx/device_raw.h>
#include <nuttx/power/pm.h>
#include <nuttx/time.h>
#include <nuttx/wqueue.h>
#include <arch/board/mods.h>

#include "hdk.h"
#include "stm32_tim.h"

#define ADC_ACTIVITY    10
#define ADC_TIM          6
#define ADC_TIM_FREQ  80000000UL
#define ADC_PERIOD    800

// Definitions of GPIOs nuttx/nuttx/configs/hdk/muc/include/mods.h
// PINS ref for perforated board https://developer.motorola.com/build/mdk-user-guide/perforated-board#electrical
#define LED_ON              0
#define LED_OFF             1

#ifdef CONFIG_STM32_ADC1
/* The number of ADC channels in the conversion list */
#define ADC1_NCHANNELS 1

#define DATA_LEN 1000
#define BUFFER_LEN (DATA_LEN*18)

sem_t               mutex;      /* Only one thread can access at a time */

uint8_t adc_data[BUFFER_LEN];
uint16_t idx_tail = 0, data_size = 0;

/* Identifying number of each ADC channel
 * oscilloscope output is available on PC3, which corresponds
 * to ADC1 channel 4 (GPIO_ADC1_IN4).
 */
static const uint8_t  g_chanlist[ADC1_NCHANNELS] = {4};

/* Configurations of pins used by each ADC channel */
static const uint32_t g_pinlist[ADC1_NCHANNELS]  = {GPIO_ADC1_IN4};
#endif

static struct work_s data_report_work; /* work queue for data reporting */
static struct work_s data_send_work; /* work queue for data send to phone */
long counter = 0;

struct device *mDev = NULL;

bool sendToDev = false;

/*
 * Callback to send data from Mod to Application
 * the function definition from device_raw.h is:
 * typedef int (*raw_send_callback)(struct device *dev, uint32_t len, uint8_t data[]);
 */
raw_send_callback mCallback = NULL;

static struct stm32_tim_dev_s *tim_dev;
bool led_state = 0;
static int adc_timer_handler(int irq, FAR void *context)
{

    pm_activity(ADC_ACTIVITY);
    STM32_TIM_ACKINT(tim_dev, 0);
    
    led_state = ~led_state + 2;
    gpio_set_value(GPIO_MODS_LED_DRV_3, led_state);

    return 0;
}

static void adc_timer_start(void)
{
    gpio_direction_out(GPIO_MODS_LED_DRV_3, 0);
    gpio_direction_out(GPIO_MODS_DEMO_ENABLE, 0);

    gpio_set_value(GPIO_MODS_DEMO_ENABLE, 0);
    gpio_set_value(GPIO_MODS_LED_DRV_3, 1);
    if (!tim_dev) {
        dbg("ADC timer\n");

        tim_dev = stm32_tim_init(ADC_TIM);

        DEBUGASSERT(tim_dev);

        STM32_TIM_SETPERIOD(tim_dev, ADC_PERIOD);
        STM32_TIM_SETCLOCK(tim_dev, ADC_TIM_FREQ);
        STM32_TIM_SETMODE(tim_dev, STM32_TIM_MODE_PULSE);
        STM32_TIM_SETISR(tim_dev, adc_timer_handler, 0);
        STM32_TIM_ENABLEINT(tim_dev, 0);
    } else {
        dbg("ignore\n");
    }
}

static void adc_timer_stop(void)
{
    gpio_set_value(GPIO_MODS_DEMO_ENABLE, 0);

    if (tim_dev) {
        dbg("STOP adc timer\n");

        STM32_TIM_DISABLEINT(tim_dev, 0);
        stm32_tim_deinit(tim_dev);
        tim_dev = NULL;

        gpio_set_value(GPIO_MODS_LED_DRV_3, LED_OFF);
    } else {
        dbg("ignore\n");
    }
}


/**
 * Initialize ADC1 interface and register the ADC driver.
 */
static int adc_devinit(void)
{
#ifdef CONFIG_STM32_ADC1
    static bool initialized = false;
    struct adc_dev_s *adc;
    int ret;
    int i;

    /* Check if we have already initialized */
    if (!initialized)
    {
        /* Configure the pins as analog inputs for the selected channels */
        for (i = 0; i < ARRAY_SIZE(g_pinlist); i++)
        {
            stm32_configgpio(g_pinlist[i]);
        }

        /* Call stm32_adcinitialize() to get an instance of the ADC interface */
        adc = stm32_adcinitialize(1, g_chanlist, ARRAY_SIZE(g_chanlist));
        if (adc == NULL)
        {
            adbg("ERROR: Failed to get ADC interface\n");
            return -ENODEV;
        }

        /* Register the ADC driver */
        ret = adc_register(TEMP_RAW_ADC_DEVPATH, adc);
        if (ret < 0)
        {
            adbg("adc_register failed: %d\n", ret);
            return ret;
        }

        /* Now we are initialized */
        adbg("adc initialized: %s\n", TEMP_RAW_ADC_DEVPATH);
        initialized = true;
    }

    return OK;
#else
    return -ENOSYS;
#endif
}

/**
 * worker function for the scheduled work queue.
 * open ADC device and trigger a sample reading.
 */
static void osc_raw_worker(void *arg)
{
    int ret;
    int adc_fd;
    struct adc_msg_s sample;
    adc_fd = open(TEMP_RAW_ADC_DEVPATH, O_RDONLY);
    
    if (adc_fd < 0)
    {
        dbg("open %s failed: %d\n", TEMP_RAW_ADC_DEVPATH, errno);
    }
    int i = 0;
    for (; i < DATA_LEN; i ++) {
        ret = ioctl(adc_fd, ANIOC_TRIGGER, 0);
        if (ret < 0)
        {
            dbg("ANIOC_TRIGGER ioctl failed: %d\n", errno);
        }

        ret = read(adc_fd, &sample, sizeof(sample));

        sem_wait(&mutex);        
        adc_data[(idx_tail+data_size)%BUFFER_LEN] = sample.am_data;
        data_size++;
        //dbg("data_size = %d, idx_tail = %d\n", data_size, idx_tail);
        // check overflow
        if (data_size > BUFFER_LEN) {
            //dbg("OVERFLOW!!! data_size = %d, idx_tail = %d\n", data_size, idx_tail);
            idx_tail = (idx_tail + DATA_LEN)%BUFFER_LEN;
            data_size = BUFFER_LEN - DATA_LEN;
        }
        sem_post(&mutex);

        if (counter % 10000 == 0) {
            dbg("len = %d, value = %d\n", ret, sample.am_data);
            //dbg("adc_fd: %d", adc_fd);
        }
        counter ++;
    }

    
    close(adc_fd);
    /* schedule work */
    work_queue(LPWORK, &data_report_work,
                osc_raw_worker, arg, 0);
    return;
}

static void send_raw_worker(void *arg) {
    //dbg("Working!!");
    //send data if mod is attached, phone request data and we have enough data to send
    if (mCallback != NULL && mDev != NULL && sendToDev && data_size > DATA_LEN) {
        mCallback(mDev, DATA_LEN, adc_data+idx_tail);

        sem_wait(&mutex);     
        idx_tail = (idx_tail + DATA_LEN)%BUFFER_LEN;
        data_size -= DATA_LEN;
        sem_post(&mutex);

        dbg("Sending!! idx_tail = %d, data_size = %d\n", idx_tail, data_size);
    }

    /* schedule work */
    work_queue(LPWORK, &data_send_work,
        send_raw_worker, NULL, 0.1);
}

/*
 * Receiver callback function
 * When this is called??
 */
static int recv_callback(struct device *dev, uint32_t len, uint8_t data[])
{   
    //if we have empty data
    if (len == 0) {
        return -EINVAL;
	}

	//turn the led on/off based on incoming data
    if (data[0] == 0 || data[0] == '0') {
        gpio_set_value(GPIO_MODS_LED_DRV_3, LED_OFF);
        dbg("Send disabled\n");
        sendToDev = false;
	} else {
        gpio_set_value(GPIO_MODS_LED_DRV_3, LED_ON);
        dbg("Send enabled\n");
        sendToDev = true;
	}
	
    return 0;
}

/*
 * Callback called when device is attached
 * We should save the "raw_send_callback" because we'll use it to
 * send data from Mod to Application
 */
static int register_callback(struct device *dev, raw_send_callback callback)
{
	//Save the callback
    mCallback = callback;
    
    mDev = dev;

    /* Initialize ADC */
    adc_devinit();

    // mutex
    sem_init(&mutex, 0, 1);

    /* cancel any work and reset ourselves */
    if (!work_available(&data_report_work))
        work_cancel(LPWORK, &data_report_work);
    /* schedule work */
    work_queue(LPWORK, &data_report_work,
                osc_raw_worker, NULL, 0);
    
    /* cancel any work and reset ourselves */
    if (!work_available(&data_send_work))
        work_cancel(LPWORK, &data_send_work);
    /* schedule work */
    work_queue(LPWORK, &data_send_work,
        send_raw_worker, NULL, 0);
    
    adc_timer_start();
    
    return 0;
}

/*
 * Callback called when device is detached
 */
static int unregister_callback(struct device *dev)
{
    sendToDev = false;
    adc_timer_stop();

    sem_destroy(&mutex);

    if (!work_available(&data_report_work))
        work_cancel(LPWORK, &data_report_work);
    /* Nothing to do */
    return 0;
}

/*
 * Callback called when MOD powers on
 */
static int probe_callback(struct device *dev)
{
    gpio_direction_out(GPIO_MODS_LED_DRV_3, LED_OFF);
    gpio_direction_out(GPIO_MODS_DEMO_ENABLE, 1);

    return 0;
}

/*
 * Here we assign our functions to the callback struct
 * defined in include/nuttx/device_raw.h
 * 
 * To Study: Need to understand how these callback works
 * There's the register/unregister which are called when the mod is attached or detached.
 * But what is the recv (Receive) callback?? When it is called??
 */
static struct device_raw_type_ops health_type_ops = {
    .recv = recv_callback,
    .register_callback = register_callback,
    .unregister_callback = unregister_callback,
};

/*
 * Assign the Probe Callback (which is called when the Mod is powered)
 * Assign the type_ops struct
 */
static struct device_driver_ops health_driver_ops = {
    .probe = probe_callback,
    .type_ops = &health_type_ops,
};

/*
 * Define driver to the program
 * This structure will be referenced from OS
 * P.S.: .name should be the same in stm32_boot struct definition
 */
struct device_driver mods_raw_health_driver = {
    .type = DEVICE_TYPE_RAW_HW,
    .name = "mods_health",
    .desc = "Health Mod Driver",
    .ops = &health_driver_ops,
};
