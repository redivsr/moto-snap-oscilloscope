/*
 * Copyright (c) 2016 Motorola Mobility, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <debug.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <arch/byteorder.h>
#include <arch/board/board.h>

#include <nuttx/arch.h>
#include <nuttx/analog/adc.h>
#include <nuttx/clock.h>
#include <nuttx/gpio.h>
#include <nuttx/greybus/debug.h>
#include <nuttx/device.h>
#include <nuttx/device_raw.h>
#include <nuttx/power/pm.h>
#include <nuttx/time.h>
#include <nuttx/wqueue.h>
#include <arch/board/mods.h>

#include "hdk.h"
#include "stm32_tim.h"

#define BLINKY_ACTIVITY    10
#define BLINKY_TIM          6
#define BLINKY_TIM_FREQ  1000
#define BLINKY_PERIOD    1000

#define LED_ON              0
#define LED_OFF             1

/* Command IDs */
#define RAW_COMMAND_DATA     0x04

#define RAW_COMMAND_RESP_MASK    0x80

#ifdef CONFIG_STM32_ADC1
/* The number of ADC channels in the conversion list */
#define ADC1_NCHANNELS 1

/* Identifying number of each ADC channel
 * oscilloscope output is available on PC3, which corresponds
 * to ADC1 channel 4 (GPIO_ADC1_IN4).
 */
static const uint8_t  g_chanlist[ADC1_NCHANNELS] = {4};

/* Configurations of pins used by each ADC channel */
static const uint32_t g_pinlist[ADC1_NCHANNELS]  = {GPIO_ADC1_IN4};
#endif

struct data_raw_info {
    struct device *gDevice;         /* device handle for this driver */
    uint16_t interval;              /* reporting interval */
    raw_send_callback gCallback;    /* callback to send back messages */
    uint8_t client_verified;        /* flag to indicate client was verified */
};

static struct work_s data_report_work; /* work queue for data reporting */
long counter = 0;

/*
 * Message structure between this driver and client.
 * |   1-byte   |  1-byte           |   N bytes         |
 * |   CMD ID   |  PAYLOAD SIZE (N) |   PAYLOAD  DATA   |
 */
struct data_raw_msg {
    uint8_t     cmd_id;
    uint8_t     size;
    uint8_t     payload[];
} __packed;

static struct stm32_tim_dev_s *tim_dev;
bool led_state = 0;
static int blinky_timer_handler(int irq, FAR void *context)
{

    pm_activity(BLINKY_ACTIVITY);
    STM32_TIM_ACKINT(tim_dev, 0);
    
    led_state = ~led_state + 2;
    gpio_set_value(GPIO_MODS_LED_DRV_3, led_state);

    return 0;
}

static void blinky_timer_start(void)
{
    gpio_direction_out(GPIO_MODS_LED_DRV_3, 0);
    gpio_direction_out(GPIO_MODS_DEMO_ENABLE, 0);

    gpio_set_value(GPIO_MODS_DEMO_ENABLE, 0);
    gpio_set_value(GPIO_MODS_LED_DRV_3, 1);
    if (!tim_dev) {
        dbg("BLINKY\n");

        tim_dev = stm32_tim_init(BLINKY_TIM);

        DEBUGASSERT(tim_dev);

        STM32_TIM_SETPERIOD(tim_dev, BLINKY_PERIOD);
        STM32_TIM_SETCLOCK(tim_dev, BLINKY_TIM_FREQ);
        STM32_TIM_SETMODE(tim_dev, STM32_TIM_MODE_PULSE);
        STM32_TIM_SETISR(tim_dev, blinky_timer_handler, 0);
        STM32_TIM_ENABLEINT(tim_dev, 0);
    } else {
        dbg("ignore\n");
    }
}

/**
 * Initialize ADC1 interface and register the ADC driver.
 */
static int adc_devinit(void)
{
#ifdef CONFIG_STM32_ADC1
    static bool initialized = false;
    struct adc_dev_s *adc;
    int ret;
    int i;

    /* Check if we have already initialized */
    if (!initialized)
    {
        /* Configure the pins as analog inputs for the selected channels */
        for (i = 0; i < ARRAY_SIZE(g_pinlist); i++)
        {
            stm32_configgpio(g_pinlist[i]);
        }

        /* Call stm32_adcinitialize() to get an instance of the ADC interface */
        adc = stm32_adcinitialize(1, g_chanlist, ARRAY_SIZE(g_chanlist));
        if (adc == NULL)
        {
            adbg("ERROR: Failed to get ADC interface\n");
            return -ENODEV;
        }

        /* Register the ADC driver */
        ret = adc_register(TEMP_RAW_ADC_DEVPATH, adc);
        if (ret < 0)
        {
            adbg("adc_register failed: %d\n", ret);
            return ret;
        }

        /* Now we are initialized */
        adbg("adc initialized: %s\n", TEMP_RAW_ADC_DEVPATH);
        initialized = true;
    }

    return OK;
#else
    return -ENOSYS;
#endif
}

/**
 * Call the base raw device to send our data
 */
static int data_raw_send(struct device *dev, uint32_t len, uint8_t data[])
{
    struct data_raw_info *info = NULL;

    if (!dev || !device_get_private(dev)) {
        return -ENODEV;
    }
    info = device_get_private(dev);

    if (info->gCallback) {
        info->gCallback(info->gDevice, len, data);
    }
    return OK;
}

/**
 * build and send message with payload data.
 */
static int data_raw_build_send_mesg(struct device *dev, uint8_t cmd_id,
                        uint8_t size, uint8_t *data, uint8_t resp)
{
    int ret = 0;
    uint8_t *resp_msg;
    struct data_raw_msg  *sresp;
    int resp_size = sizeof(struct data_raw_msg) + size;

    if (size == 0) return -EINVAL;

    /* allocate memory for response message */
    resp_msg = zalloc(resp_size);
    if (!resp_msg) {
        return -ENOMEM;
    }

    /* fill in the message structure */
    sresp = (struct data_raw_msg *)resp_msg;
    sresp->cmd_id = cmd_id;
    if (resp)
        sresp->cmd_id |= RAW_COMMAND_RESP_MASK;
    sresp->size = size;
    /* copy payload data and send the message */
    memcpy((void *)&sresp->payload[0], data, size);
    ret = data_raw_send(dev, resp_size, resp_msg);

    if (ret != OK) {
        dbg("send ret = %d\n", ret);
    }

    free(resp_msg);

    return ret;
}

/**
 * worker function for the scheduled work queue.
 * open ADC device and trigger a sample reading.
 */
static void osc_raw_worker(void *arg)
{
    int ret;
    int adc_fd;
    struct adc_msg_s sample;
    struct data_raw_info *info = NULL;

    info = arg;

    adc_fd = open(TEMP_RAW_ADC_DEVPATH, O_RDONLY);
    if (adc_fd < 0)
    {
        dbg("open %s failed: %d\n", TEMP_RAW_ADC_DEVPATH, errno);
    }

    while (1) {
        ret = ioctl(adc_fd, ANIOC_TRIGGER, 0);
        if (ret < 0)
        {
            dbg("ANIOC_TRIGGER ioctl failed: %d\n", errno);
        }

        ret = read(adc_fd, &sample, sizeof(sample));
        counter ++;
        if (counter % 100000 == 0) {
            /*if (sample.am_data > 100)
                gpio_set_value(GPIO_MODS_LED_DRV_3, 0);
            else
                gpio_set_value(GPIO_MODS_LED_DRV_3, 1);*/
            dbg("a = %d, value = %d\n", ret, sample.am_data);
        }
        // send message
        data_raw_build_send_mesg(info->gDevice, RAW_COMMAND_DATA,
            sizeof(sample.am_data), (uint8_t *)&sample.am_data, 0);
    }

    close(adc_fd);

    /* cancel any work and reset ourselves */
    if (!work_available(&data_report_work))
        work_cancel(LPWORK, &data_report_work);

    /* schedule work */
    work_queue(LPWORK, &data_report_work,
                osc_raw_worker, NULL, 0);
    return;
}

/*
 * Prepare callback from power management.
 * This driver returns non-zero value when data
 * reporting is enabled.
 */
#ifdef CONFIG_PM
static int pm_prepare(struct pm_callback_s *cb, enum pm_state_e state)
{
    /*
    * Do not allow IDLE when work is scheduled
    */
    if ((state >= PM_IDLE) && (!work_available(&data_report_work)))
        return -EIO;

    return OK;
}

static struct pm_callback_s pm_callback =
{
  .prepare = pm_prepare,
};
#endif

/**
 * We got data from the device (core) side.
 */
static int osc_raw_recv(struct device *dev, uint32_t len, uint8_t data[])
{
    return 0;
}

/**
 * register the send callback function.
 */
static int osc_raw_register_callback(struct device *dev,
        raw_send_callback callback)
{
    struct data_raw_info *info = NULL;

    if (!dev || !device_get_private(dev)) {
        return -ENODEV;
    }
    info = device_get_private(dev);

    info->gCallback = callback;
    return 0;
}

/**
 * unregister the send callback function.
 */
static int osc_raw_unregister_callback(struct device *dev)
{
    struct data_raw_info *info = NULL;

    if (!dev || !device_get_private(dev)) {
        return -ENODEV;
    }
    info = device_get_private(dev);

    info->gCallback = NULL;
    return 0;
}

/**
 * probe function called upon device registration.
 */
static int osc_raw_probe(struct device *dev)
{
    struct data_raw_info *info;

    if (!dev) {
        return -EINVAL;
    }

    info = zalloc(sizeof(*info));
    if (!info) {
        return -ENOMEM;
    }

    info->client_verified = 0;
    info->gDevice = dev;
    device_set_private(dev, info);

    /* Enable oscilloscope device */
    gpio_direction_out(GPIO_MODS_DEMO_ENABLE, 0);
                    info->client_verified = 1;
    blinky_timer_start();
    /* Initialize ADC */
    adc_devinit();

    /* cancel any work and reset ourselves */
    if (!work_available(&data_report_work))
        work_cancel(LPWORK, &data_report_work);

    /* schedule work */
    work_queue(LPWORK, &data_report_work,
                osc_raw_worker, info, 0);

#ifdef CONFIG_PM
    if (pm_register(&pm_callback) != OK)
    {
        dbg("Failed register to power management!\n");
    }
#endif

    llvdbg("Probe complete\n");

    return 0;
}

/**
 * remove function called when device is unregistered.
 */
static void osc_raw_remove(struct device *dev)
{
    struct data_raw_info *info = NULL;

    if (!dev || !device_get_private(dev)) {
        return;
    }
    info = device_get_private(dev);

    free(info);
    device_set_private(dev, NULL);
}

static struct device_raw_type_ops osc_raw_type_ops = {
    .recv = osc_raw_recv,
    .register_callback = osc_raw_register_callback,
    .unregister_callback = osc_raw_unregister_callback,
};

static struct device_driver_ops oscilloscope_driver_ops = {
    .probe = osc_raw_probe,
    .remove = osc_raw_remove,
    .type_ops = &osc_raw_type_ops,
};

struct device_driver mods_raw_oscilloscope_driver = {
    .type = DEVICE_TYPE_RAW_HW,
    .name = "mods_raw_oscilloscope",
    .desc = "Oscilloscope Raw Interface",
    .ops = &oscilloscope_driver_ops,
};
