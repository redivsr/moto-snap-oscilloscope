
### How do I set up and make a project? ###

* On nuttx folder create a blank project based on blinky example:
	* $ ./create_project.sh example
		* It will create a folder on projects/example/ with symbolic links to the project files
* On nuttx/nuttx folder run:
	* $ make distclean
* Enter nuttx/nuttx/tools folder and configure the build to point to yours:
	* $ ./configure hdk/muc/example
* On nuttx/nuttx folder run:
	* $ make menuconfig
		* Make sure your project is selected on "Board Selection", and Application Configuration > Greybus utility > manifest name contains your manifest name
* Build the project using:
	* $ make
		* It will create nuttx.tftf file
* Flash your binary into the MOD:
	* $ openocd -f board/moto_mdk_muc_reset.cfg -c "program nuttx.tftf 0x08008000 reset exit"