
/*
 * Copyright (c) 2016 Motorola Mobility, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <debug.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <arch/byteorder.h>
#include <arch/board/board.h>

#include <nuttx/arch.h>
#include <nuttx/analog/adc.h>
#include <nuttx/clock.h>
#include <nuttx/gpio.h>
#include <nuttx/greybus/debug.h>
#include <nuttx/device.h>
#include <nuttx/device_raw.h>
#include <nuttx/power/pm.h>
#include <nuttx/time.h>
#include <nuttx/wqueue.h>
#include <arch/board/mods.h>


#include "hdk.h"

#ifdef CONFIG_STM32_ADC1
/* The number of ADC channels in the conversion list */
#define ADC1_NCHANNELS 1

/* Identifying number of each ADC channel
 * Temperature output is available on PC3, which corresponds
 * to ADC1 channel 4 (GPIO_ADC1_IN4).
 */
static const uint8_t  g_chanlist[ADC1_NCHANNELS] = {4};

/* Configurations of pins used by each ADC channel */
static const uint32_t g_pinlist[ADC1_NCHANNELS]  = {GPIO_ADC1_IN4};
#endif

static struct work_s data_report_work; /* work queue for data reporting */

/**
 * worker function for the scheduled work queue.
 * open ADC device and trigger a sample reading.
 */
static void osc_raw_worker(void *arg)
{
    int ret;
    int adc_fd;
    struct adc_msg_s sample;

    adc_fd = open(TEMP_RAW_ADC_DEVPATH, O_RDONLY);
    if (adc_fd < 0)
    {
        dbg("open %s failed: %d\n", TEMP_RAW_ADC_DEVPATH, errno);
    }

    for (;;) {
        ret = ioctl(adc_fd, ANIOC_TRIGGER, 0);
        if (ret < 0)
        {
            dbg("ANIOC_TRIGGER ioctl failed: %d\n", errno);
        }

        read(adc_fd, &sample, sizeof(sample));
    }
    close(adc_fd);

    return;
}

/**
 * Initialize ADC1 interface and register the ADC driver.
 */
static int adc_devinit(void)
{
#ifdef CONFIG_STM32_ADC1
    static bool initialized = false;
    struct adc_dev_s *adc;
    int ret;
    int i;

    /* Check if we have already initialized */
    if (!initialized)
    {
        /* Configure the pins as analog inputs for the selected channels */
        for (i = 0; i < ARRAY_SIZE(g_pinlist); i++)
        {
            stm32_configgpio(g_pinlist[i]);
        }

        /* Call stm32_adcinitialize() to get an instance of the ADC interface */
        adc = stm32_adcinitialize(1, g_chanlist, ARRAY_SIZE(g_chanlist));
        if (adc == NULL)
        {
            adbg("ERROR: Failed to get ADC interface\n");
            return -ENODEV;
        }

        /* Register the ADC driver */
        ret = adc_register(TEMP_RAW_ADC_DEVPATH, adc);
        if (ret < 0)
        {
            adbg("adc_register failed: %d\n", ret);
            return ret;
        }

        /* Now we are initialized */
        adbg("adc initialized: %s\n", TEMP_RAW_ADC_DEVPATH);
        initialized = true;
    }
    /* cancel any work and reset ourselves */
    if (!work_available(&data_report_work))
        work_cancel(LPWORK, &data_report_work);

    /* schedule work */
    work_queue(LPWORK, &data_report_work,
                osc_raw_worker, NULL, 0);
    return OK;
#else
    return -ENOSYS;
#endif
}

/**
 * We got data from the device (core) side.
 */
static int oscilloscope_raw_recv(struct device *dev, uint32_t len, uint8_t data[])
{
    return 0;
}

/*
 * Prepare callback from power management.
 * This driver returns non-zero value when data
 * reporting is enabled.
 */
#ifdef CONFIG_PM
static int pm_prepare(struct pm_callback_s *cb, enum pm_state_e state)
{
    /*
    * Do not allow IDLE when work is scheduled
    */
    if ((state >= PM_IDLE) && (!work_available(&data_report_work)))
        return -EIO;

    return OK;
}

static struct pm_callback_s pm_callback =
{
  .prepare = pm_prepare,
};
#endif

/**
 * We got data from the device (core) side.
 */
static int osc_raw_recv(struct device *dev, uint32_t len, uint8_t data[])
{
    return 0;
}

/**
 * register the send callback function.
 */
static int oscilloscope_raw_register_callback(struct device *dev,
        raw_send_callback callback)
{
    if (!dev || !device_get_private(dev)) {
        return -ENODEV;
    }
    return 0;
}

/**
 * unregister the send callback function.
 */
static int oscilloscope_raw_unregister_callback(struct device *dev)
{
    if (!dev || !device_get_private(dev)) {
        return -ENODEV;
    }
    return 0;
}

/**
 * probe function called upon device registration.
 */
static int oscilloscope_raw_probe(struct device *dev)
{
    if (!dev) {
        return -EINVAL;
    }

    /* Enable temperature device */
    gpio_direction_out(GPIO_MODS_DEMO_ENABLE, 0);
    adc_devinit();

#ifdef CONFIG_PM
    if (pm_register(&pm_callback) != OK)
    {
        dbg("Failed register to power management!\n");
    }
#endif

    llvdbg("Probe complete\n");

    return 0;
}

/**
 * remove function called when device is unregistered.
 */
static void oscilloscope_raw_remove(struct device *dev)
{
    if (!dev || !device_get_private(dev)) {
        return;
    }
    device_set_private(dev, NULL);
}

static struct device_raw_type_ops oscilloscope_raw_type_ops = {
    .recv = oscilloscope_raw_recv,
    .register_callback = oscilloscope_raw_register_callback,
    .unregister_callback = oscilloscope_raw_unregister_callback,
};

static struct device_driver_ops oscilloscope_driver_ops = {
    .probe = oscilloscope_raw_probe,
    .remove = oscilloscope_raw_remove,
    .type_ops = &oscilloscope_raw_type_ops,
};

struct device_driver mods_raw_oscilloscope_driver = {
    .type = DEVICE_TYPE_RAW_HW,
    .name = "mods_raw_temperature",
    .desc = "Oscilloscope Raw Interface",
    .ops = &oscilloscope_driver_ops,
};
